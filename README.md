# 贡献



# 分布式游戏业务框架（模板） node-gws
- todolist 进度 30%
- contorller-service-modeData
- web/socket->grpc

# 基础架构
- [X] 服务发现：全部服务注册和健康检查
- [] ~~服务发现：配置监听和修改~~
- [x] web层：koa2+ts-pb+controller 
- [x] web层：Post+JSON+pbtype;
- [x] web层：grpc-web
- [ ] ~~web层：Get请求+pbtype;~~
- [ ] web层：Post+from表单处理文件上传 或 (pb 处理流文件)
- [ ] web层：Post+二进制传输+解析成pb+PB包一层+固定路由/二进制->转发到grpc(这样做的好处是无需关注proto的新增和更新) ->grpc业务微服务->返回rsp  doing
- [ ] web层：JWT鉴权
- [X] grpc层：grpc服务启动 
- [X] grpc层：grpc微服务注册和发现
- [X] grpc层：grpc微服务分布式部署和请求返回
- [ ] socket层：使用业务微服务+webtoken JWT进行鉴权
- [ ] socket层：socket-client-req->socket微服务 ->pb-> 业务微服务-> socket微服务->rsp返回
- [ ] socket层：业务微服务-> 发起广播->socket微服务-> socket-> client
- [ ] 有状态服务web转发： session缓存->web-client-req->socket微服务 ->pb-> 查询用户所分配的解析微服务组名对应的IP -> 指定的业务微服务
- [ ] 有状态服务socket转发：session缓存->socket-client-req->socket微服务 ->pb-> 查询用户所分配的解析微服务组名对应的IP -> 指定的业务微服务
# 基础组件
- [ ] 工具-本地过期缓冲
- [ ] 工具-定时器
- [ ] 工具-自然语言库
- [ ] 工具：jwt头部鉴权和验证+获取user信息 doing
- [ ] 工具 oauth2.0 https://www.jetbrains.com/help/webstorm/oauth-2-0-authorization.html
- [ ] 业务-用户web登录和socket登录验证鉴权
- [ ] 业务-socket广播
- [ ] 另选一个服务发现Zookeeper 
# 业务模板
- [ ] 游戏-小游戏后端流程生命周期模板（这个可以会写在另外一个库）（cocos+node-gws） (unity+net-gws) (godot+go-gws)
- [ ] 游戏-模拟小游戏前端流程的压测模块（这个可以会写在另外一个库）
- [ ] 游戏-匹配服务
- [ ] 游戏-box2d物理检查 https://box2d.org/documentation_v3/md_hello.html#autotoc_md21
- [ ] ~~游戏-P2物理 https://github.com/schteppe/p2.js~~
- [ ] 游戏-AOI广播算法
- [ ] 游戏-回合同步模板
- [ ] 游戏-帧同步模板
- [ ] 游戏-寻路
- [ ] 游戏-行为树
- [ ] 游戏-ecs设计
- [ ] AI-Gpt的对接
- [ ] AI-智能体的对接
- [ ] AI-AI绘画SD的对接
- [ ] 游戏常用基础算法
- [ ] 游戏人工只能算法
- [ ] 游戏高级定时器模块设计
- [ ] 游戏房间和人数匹配模块
- [ ] 游戏任务设计模块
- [ ] 游戏成就设计模块
- [ ] 游戏背包设计模块
- [ ] 游戏装扮设计模块
- [ ] 远程修改配置
# 中间间支持
- [ ] 中间件-mongodb
- [ ] 中间件-redis
- [ ] 中间件-mysql
- [ ] 中间件-kafka
- [ ] 中间件-ftp
- [ ] 中间件-大数据数据库
- [ ] 中间件-搜索引擎 Milvus
- [ ] 中间件-向量数据库
- [ ] 协议支持KCP
# CICD
- [X] CICD-开发环境package.json; dev热更新运行，每个服务的运行参考案例
- [X] CICD-生产pm2多核运行部署
- [ ] CICD-docker部署
- [ ] CICD-k8s服务网格发现
- [ ] CICD-k8s部署指南 pod运行
# 运行时
- [ ]Node.js：如果你更倾向于稳定可靠、经过多年检验的生态系统，Node.js 是不错的选择。
- [ ]Deno：如果你重视安全性和最新的编程环境特性，推荐使用 Deno。它还支持开箱即用的 TypeScript。
- [ ]Bun：如果你需要高速性能，尤其是在处理 JavaScript 或 TypeScript 时，Bun 是一个合适的选择。 
# 测试和压测
- [ ] 相应的客户端测试代码
- [ ] 压测游戏代码模板
- [x] 接口测试模板

# 安装protoc
https://github.com/protocolbuffers/protobuf/releases
使用nice-grpc

# consul 安装
https://kingfree.gitbook.io/consul/install
- 下载和安装
- 命令放path
- consul agent -dev -ui
- http://localhost:8500/ui


# socket
10. socket-client-req->socket微服务->pb-> 业务微服务-》返回
11. 登录socket
12. 缓存用户连接，组/房间概念
13. 业务微服务发起广播->socket微服务

# http和socket公用同一个端口
const Koa = require('koa')
const app = new Koa()
const path = require('path')
const ws = require('ws')

app.use(require('koa-static')(path.join(__dirname) + '/public'))

let server = app.listen(4000, () => {
let port = server.address().port
console.log('应用实例，访问地址为 http://localhost:' + port)
})

const wss = new ws.Server({ server })
wss.on('connection', function connection(ws) {
ws.on('message', function incoming(message) {
console.log('received: %s', message)
})
})