import * as path from "path";
import {ConsulOptions} from "consul";

 class dev {
    // 初始化 koa2 插件需要的一些配置
    // 本地Mysql数据库配置
    typeorm = {
        type: 'mysql',
        host: "localhost",
        charset: "utf8_general_ci",
        port: 3306,
        username: "root",
        password: "user123",
        database: "game",
        logging: "all",
        synchronize: false,
        logger: "advanced-console",
        cache: {
            duration: 30000
        },
        maxQueryExecutionTime: 1000,
        entities: [
            `${path.join(__dirname,'../entity/*{.js,.ts}')}`
        ]
    }
    consul:ConsulOptions = {
        host:"10.112.139.131",
        port: "19500",
        promisify: true,
    }
    // jwt 配置
    tokenSecret: string = "12345";
    webTokenTimeOut = 1000000;
}
 class prod extends dev  {
     // 初始化 koa2 插件需要的一些配置
     // 本地Mysql数据库配置
     consul ={
         host:"",
         port: "",
         promisify: true,
     }
     tokenSecret: string = "12345";
};
export const ServiceConst = process.env.NODE_ENV == "prod" ? new prod() : new dev() ;