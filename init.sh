#pnpm全局仓库路径
pnpm config set store-dir "/Users/lili/pnpm/.pnpm-store"
#pnpm全局安装路径
pnpm config set global-dir "/Users/lili/pnpm/pnpm-global"
#pnpm 全局bin路径 ，这个路径还要配置到环境变量里面去
pnpm config set global-bin-dir "/Users/lili/pnpm/bin"
#pnpm创建pnpm-satate.json文件的目录
pnpm config set state-dir "/Users/lili/pnpm/pnpm/state"
#pnpm全局缓存路径
pnpm config set cache-dir "/Users/lili/pnpm/cache"

pnpm install

