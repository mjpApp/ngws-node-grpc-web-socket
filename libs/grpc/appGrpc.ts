import {createChannel, createClient, createServer, Server} from 'nice-grpc';
import {ServiceType} from "../../pb/node/base";
import {logger} from "../utils/logs";
import {CompatServiceDefinition} from "nice-grpc/lib/service-definitions";
import {ProcessService} from "../utils/processService";
import {ClientGrpc} from "./clientGrpc";

// await
// 压测 ghz   grpcui
export class AppGrpc {
    private grpcServer:Server;
    private serviceName: string;
    private port: number;
    constructor() {
        this.grpcServer = createServer();
    }
    async run(serviceType:ServiceType, definition:CompatServiceDefinition, imp, port?:number){
        this.serviceName = ServiceType[serviceType];
        this.grpcServer.add(definition, new imp());
        this.port = port || serviceType;
        logger.info('启动grpc服务', this.port, ServiceType[serviceType])
        await this.grpcServer.listen('0.0.0.0:' + (this.port));
        ClientGrpc.registerGrpc(definition.fullName as string,this.port)
        ProcessService.pushEndEvent(async ()=>{
            await this.grpcServer.shutdown();
            logger.info("关闭grpc")
        })
    }

    // async runHealthService( serviceType: number ){
    //     // 实现健康检查端点
    //     logger.info('启动检测服务',serviceType+10000)
    //     this.httpServer = http.createServer((req, res) => {
    //         // 检查gRPC服务的健康状态
    //         // 如果服务健康，返回200 OK；否则返回500 Internal Server Error
    //         res.writeHead(200);
    //         res.end();
    //     });
    //     return new Promise(resolve => {
    //         this.httpServer.listen( serviceType+10000 , '127.0.0.1', () => {
    //             logger.info('Health check server started, listening on port ' + (serviceType+10000));
    //             resolve(true);
    //         });
    //     })
    // }
}

