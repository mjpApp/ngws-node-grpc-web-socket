import {CompatServiceDefinition} from "nice-grpc/lib/service-definitions";
import {logger} from "../utils/logs";
import {createChannel, createClient, createServer} from "nice-grpc";
import {Rule} from "../utils/rule";
import {utils} from "../utils/utils";
import {ConsulUtil} from "./consulUtil";
export class ClientGrpc {
    private static consulUtil: ConsulUtil; // 服务发现
    private static clientGrpcMap:Map<string, any> = new Map()
    private static clientGrpcList:Map<string, string[]>   = new Map()
    static init(){
        if(!this.consulUtil){
            this.consulUtil = new ConsulUtil()
        }
    }
    static registerGrpc(fullName:string, port:number){
        this.consulUtil.register(fullName as string, utils.getIPAdress(), port, "grpc");
    }
    static registerWeb(fullName:string, port:number){
        this.consulUtil.register(fullName as string, utils.getIPAdress(), port, "web");
    }
    static registerSocket(fullName:string, port:number){
        this.consulUtil.register(fullName as string, utils.getIPAdress(), port, "socket");
    }
    static addClientGrpc<T>( definition:CompatServiceDefinition){
        this.consulUtil.listChange(definition.fullName as string,(serviceName:string, addressList:string[])=> {
            if(!this.clientGrpcList.get(serviceName)){
                this.clientGrpcList.set(serviceName,[])
            }
            let oldList = this.clientGrpcList.get(serviceName).concat();
            this.clientGrpcList.set(serviceName, addressList);
            for(let address of addressList){
                if(this.clientGrpcMap.has(address)){
                    continue
                }
                logger.info("add client Grpc",serviceName,address)
                const channel = createChannel(address);
                const client= createClient(definition, channel);
                this.clientGrpcMap.set(address, {
                    client:client,
                    channel:channel
                });
            }
            // 删除进程
            let delList = oldList.filter(a => !addressList.includes(a))
            for(let address of delList){
                if(this.clientGrpcMap.has(address)){
                    let service =  this.clientGrpcMap.get(address)
                    try{
                        logger.info("del client grpc",serviceName,address)
                        service?.channel.close()
                    }catch (e) {
                        logger.error(e)
                    }
                    this.clientGrpcMap.delete(address)
                }
            }
        })
    }
    static getClientGrpc<T>( fullName:string):T{
        let list = this.clientGrpcList.get(fullName as string) || [];
        let address = Rule.chooseRound(fullName as string, list)
        return this.clientGrpcMap.get(address)?.client;
    }
    static HasClientGrpc( fullName:string):boolean{
        let list = this.clientGrpcList.get(fullName) || [];
        return !!list.length;
    }
}
ClientGrpc.init()
