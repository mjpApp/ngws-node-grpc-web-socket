import {ServiceType} from "../../pb/node/base";
import {logger} from "../utils/logs";
import Consul from "consul";
import {ServiceConst} from "../../config/ServerConst";
import {ProcessService} from "../utils/processService";
// 需要部署consul, 每个grpc服务监听自己的分布式服务列表和管理请求
export class ConsulUtil {
    private watchAgent: Consul.Watch;
    private consul: Consul.Consul;
    private name: string;
    private address: string;
    private port: ServiceType;
    listenServiceTypeList: string[] = []
    private grpcServiceCache: Consul.Agent.Service.RegisterOptions;
    private isHasErrorNum: number = 0;
    private listenServiceFun = {};
    private serviceType: "grpc" | "web" | "socket";
    constructor(){
        this.consul = new Consul(ServiceConst.consul);
    }
    // 注册服务
    register(name:string, address:string, port:ServiceType, serviceType:"grpc"|"web"|"socket"){
        /**
         * name (String): 注册的服务名称
         * id (String, optional): 服务注册标识
         * tags (String[], optional): 服务标签
         * address (String, optional): 需要注册的服务地址（客户端）
         * port (Integer, optional): 需要注册的服务端口（客户端）
         * check (Object, optional): 服务的健康检查核心参数如下
         *
         * http (String): 健康检查路径, interval 参数为必须设置
         * interval (String): 健康检查频率
         * timeout (String, optional): 健康检查超时时间
         * checks (Object[], optional): 如果有多个检查的路径，可采用对象数组形式，参数参照上面的 check
         */
        this.name = name;
        this.address = address;
        this.port = port;
        this.serviceType = serviceType;
        const grpcService:Consul.Agent.Service.RegisterOptions = {
            id: `${name}-${address}-${port}`,
            name: name, //服务名字唯一标志
            address: address || '127.0.0.1', // todo 服务地址 可能要公网地址
            port: port, // gRPC服务端口
            tags: [serviceType],
            // check: {
            //     http: `http://${address}:${port}/health`,
            //     interval: '10s',
            //     status:"passing",
            //     ttl: '10s'
            // }
            check: {
                tcp: `${address}:${port}`,
                interval: '5s',
                timeout: "2s",
                ttl: '2s'
            } as any
        };
        this.grpcServiceCache = grpcService
        logger.info('注册服务发现', `${name}-${address}-${port}`)
        // 步骤 2: 使用Consul进行服务发现
        this.consul.agent.service.register(grpcService, err => {
            if (err) {
                logger.error( err );
            }else{
                logger.info('gRPC service registered with Consul');
            }
        });
        ProcessService.pushEndEvent(()=>{
            logger.info('gRPC service deregister:', this.grpcServiceCache.id);
            this.consul.agent.service.deregister(this.grpcServiceCache.id)
        })
        this.initWatchHealht(name)
    }

    listChange(fullName: string, fun: (serviceName: string, addressList: string[]) => void) {
        this.listenServiceTypeList.push(fullName)
        this.listenServiceFun[fullName] = fun;
        this.initlistWatchChange(fullName)
    }
    //监听服务变化
    initlistWatchChange(name:string){
        let watchAgent = this.consul.watch({
            method: this.consul.health.service,
            backoffFactor: 1000,
            options: {
                service: name,
                passing: true,
                timeout: 60000
            } as any
        })
        // 监听Consul中的服务变化
        watchAgent.on('change', data => {
            // 当有新的gRPC服务注册或注销时，您的应用程序将收到通知
            // 根据收到的通知更新本地的gRPC服务列表 todo
            // console.log('gRPC service list updated:', JSON.stringify(data,null,2));
            if(!data.length){
                return;
            }
            let serviceMap = {}
            for(let item of data){
                if(!serviceMap[item.Service.Service]){
                    serviceMap[item.Service.Service]= []
                }
                serviceMap[item.Service.Service].push(item.Service.Address+":"+item.Service.Port)
            }
            logger.info('service list updated:'+name, JSON.stringify(serviceMap));
            for(let serviceName in serviceMap){
                if(this.listenServiceTypeList.includes(serviceName)){
                    this.listenServiceFun[serviceName](serviceName,serviceMap[serviceName]);
                }
            }
        });
        watchAgent.on('error', error => {
            this.isHasErrorNum ++;
            console.log('service watch error:', error);
            // this.watchAgent.end()
        });
    }

    //监听服务变化
    initWatchHealht(name:string){
        let watchAgent = this.consul.watch({
            method: this.consul.health.service,
            backoffFactor: 1000,
            options: {
                service: name,
                passing: true,
                timeout: 60000
            } as any
        })
        // 监听Consul中的服务变化
        watchAgent.on('change', data => {
            // 当有新的gRPC服务注册或注销时，您的应用程序将收到通知
            // 根据收到的通知更新本地的gRPC服务列表 todo
            // console.log('gRPC service list updated:', JSON.stringify(data,null,2));
            if(!data.length && this.isHasErrorNum && this.name == name){
                // 重连
                this.register(this.name,this.address,this.port,this.serviceType)
                console.log('service re register:',this.name,this.address,this.port);
                this.isHasErrorNum = 0;
                return
            }
        });
        watchAgent.on('error', error => {
            this.isHasErrorNum ++;
            console.log('service watch error:', error);
            // this.watchAgent.end()
        });
    }

    async getConfig(key) {
        const result = await this.consul.kv.get(key) as any;
        if (!result) {
            return Promise.reject(key + '不存在');
        }
        return JSON.parse(result.Value );
    }
    // 读取 user 配置简单封装
    async getUserConfig(key) {
        const result = await this.getConfig('develop/user');
        if (!key) {
            return result;
        }
        return result[key];
    }

}


// 步骤 1: 注册gRPC服务到Consul
// 创建一个新的gRPC服务

// new ConsulUtil().register(grpcService)
// 使用Consul的注册功能将该服务注册到Consul中



