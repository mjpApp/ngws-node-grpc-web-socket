// import winston, {createLogger, format, transports} from "winston";
// import  'winston-daily-rotate-file';
import * as log4 from "koa-log4";
import path from "path";
log4.configure(
	{
		appenders:
			{
				//系统日志
				access: {
					type: 'dateFile',
					pattern: '_yyyy-MM-dd.log',
					alwaysIncludePattern: true,
					encoding:"utf-8",
					category: "access",
					filename: path.join(__dirname, '../../logs/access')
				},
				// 应用日志
				application: {
					type: 'dateFile',
					pattern: '_yyyy-MM-dd.log',
					alwaysIncludePattern: true,
					encoding:"utf-8",
					category: "application",
					filename: path.join(__dirname, '../../logs/application')
				},
				_: {
					type: 'console',
					category: "default",
					alwaysIncludePattern: true,
					encoding:"utf-8",
				}
			}
		,
		categories: {
			default: {appenders: [ '_' ], level: 'DEBUG' },
			access:  { appenders: [ 'access' ], level: 'DEBUG' },
			application: { appenders: [ 'application' ], level: 'DEBUG'}
		}
	}
);

export class Logs {
	public static AccessLogger() {
		return log4.koaLogger(log4.getLogger('access'), { level: 'auto' });
	}
}
export const logger = log4.getLogger('_')
// export const serviceName = process.env["name"] || ""
//
// const customFormat = format.combine(
// 	format.timestamp({ format: "MMM-DD-YYYY HH:mm:ss" }),
// 	format.align(),
// 	format.printf((i) => `${i.level}: ${[i.timestamp]}: ${i.message}`)
// );
// const defaultOptions = {
// 	format: customFormat,
// 	datePattern: "YYYY-MM-DD",
// 	zippedArchive: true,
// 	maxSize: "20m",
// 	maxFiles: "14d",
// };
// export const logger = createLogger({
// 	format: customFormat,
// 	transports: [
// 		new transports.Console(),
// 		new transports.DailyRotateFile({
// 			filename: `logs/${serviceName}-info-%DATE%.log`,
// 			level: "info",
// 			...defaultOptions,
// 		}),
// 		new transports.DailyRotateFile({
// 			filename: `logs/${serviceName}-error-%DATE%.log`,
// 			level: "error",
// 			...defaultOptions,
// 		}),
// 	],
// });


