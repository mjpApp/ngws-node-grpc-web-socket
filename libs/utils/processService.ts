import {utils} from "./utils";
import {logger} from "./logs";

export class ProcessService{
    static eventList = []
    static isInit = false
    static pushEndEvent(eventFun:Function){
        if(!this.isInit){
            this.initEndEvent()
        }
        this.eventList.push(eventFun)
    }
    static initEndEvent(){

        process.on('beforeExit', (code) => {
            logger.info('Process beforeExit event with code: ', code);
        });
        process.on('exit', (code) => {
            logger.info('Process exit event with code: ', code);
        });
//参数err表示发生的异常
        process.on("uncaughtException",function(err){
            logger.error(err);
        });
        process.on('SIGINT', ()=> {
            logger.info('Got SIGINT.  Press Control-D/Control-C to exit.');
            utils.doAsync(async () => {
                for(let fun of this.eventList){
                    await fun()
                }
                process.exit(0)
            }).then( r =>{})
        });
    }
}

