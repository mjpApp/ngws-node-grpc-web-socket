import {Get, Post, JsonController, Controller} from "routing-controllers";
export function RouteEntity(name: string) {
    return JsonController(name);

}
export function RouteGet(name: string = null) {
    return function (object:any, methodName:any) {
        if (!name) {
            name = "/" + methodName;
        }
        let f = Get(name)
        f(object, methodName)
    }
}
export function RoutePost(name: string = null) {
    return function (object:any, methodName:any) {
        if (!name) {
            name = "/" + methodName;
        }
        let f = Post(name)
        f(object, methodName)
    }
}


