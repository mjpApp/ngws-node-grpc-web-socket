import {CodeType,rsp} from "../../pb/node/base";

export function setJsonRsp<T extends {rsp:rsp} >(code:CodeType, rspRes:T):T{
    if(!rspRes){
        rspRes = {rsp: {}} as T;
    }
    if(!rspRes.rsp){
        rspRes.rsp = rsp.create()
    }
    rspRes.rsp.code = code;
    rspRes.rsp.msg = CodeType[code]
    return rspRes
}