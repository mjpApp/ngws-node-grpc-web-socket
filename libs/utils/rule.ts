export class Rule{
   static counterMap = new Map();
    /**
     * 通过循环规则选择一个服务节点
     * @param {String} service
     * @param {Array<String>} nodes
     * @returns 服务节点
     */
   static chooseRound(service:string, nodes:string[]) {
        let counter = this.counterMap.get(service)
        if (!counter) {
            counter = 1;
        }

        let node = nodes[counter % nodes.length];
        this.counterMap.set(service, ++counter);
        return node;
    }
    /**
     * 通过随机规则选择一个服务节点
     * @param {String} service
     * @param {Array<String>} nodes
     * @returns 服务节点
     */
    static chooseRandom(service, nodes) {
        let node = nodes[Math.floor(Math.random() * nodes.length)];
        return node;
    }
}