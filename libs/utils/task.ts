import {logger} from "./logs";

export enum RepeatType {
    Next = 0,
    Stop = 1,
}

/**
 * 定时器
 */
export class Task {
    public static NAME = "";
    public static _poolKey: string = Task.NAME;
    timer = null;
    repeatTimer = null;
    repeatCounter: number = 0;
    repeatCountTotal: number = 0;
    resolveFunc: any = null;
    userData: any = null;
    isPaused: boolean = false;
    finished: boolean = false;
    taskName = undefined;
    autoRecover: boolean = false;
    public func: (roomId: string) => void;
    eventType: number;
    private stopCallback: () => void;

    constructor(name = undefined, autoRecover: boolean = false) {
        this.taskName = name;
        this.autoRecover = autoRecover;
        if (!this.taskName) {
            this.taskName = "task";
        }
    }


    isRunning() {
        return this.timer || this.repeatTimer;
    }

    stop(clearUserData: boolean = false) {
        this.stopCallback && this.stopCallback();
        this.stopCallback= null;
        this.stopAllTimer(clearUserData);
    }

    stopAllTimer(needClearUserData = true) {
        this.stopCallback= null;
        if (this.timer) {
            clearTimeout(this.timer);
            this.timer = null;
        }
        if (this.repeatTimer) {
            clearInterval(this.repeatTimer);
            this.repeatTimer = null;
        }
        if (this.resolveFunc) {
            this.resolveFunc(null);
            this.resolveFunc = null
        }
        if (needClearUserData) {
            this.userData = null;
        }
        this.repeatCounter = 0;
        this.repeatTimer = null;
        this.repeatCountTotal = 0;
        this.taskName = "";
        this.finished = true;
    }


    /**
     * 一次延时任务
     * 任务结束后会自动回收到对象池
     * @param delayMs 延时间隔 - 毫秒
     * @param func 回调函数
     * //delay is second
     */
    oneShot(delayMs: number, func: () => void, stopCallback: () => void = null): Task {
        this.stopAllTimer(false);
        this.finished = false;
        this.stopCallback = stopCallback;
        this.timer = setTimeout(() => {
            this.timer = null;
            try {
                func()
            } catch (e) {
                logger.error("oneShot定时任务报错", e.stack)
            }
            this.stop();
        }, delayMs);
        return this;
    }


    /**
     * 周期性延时任务
     * @param intervalMs 时间间隔 - 毫秒
     * @param func 回调函数
     * @param repeatCount 周期次数 0表示无限制
     * @param stopCallback 结束回调
     * //interval is second
     */
    repeat(intervalMs, func: (dt?) => RepeatType | null | void, repeatCountTotal: number = 0, stopCallback: () => void = null): Task {
        this.stopAllTimer(false);
        this.finished = false;
        this.repeatCountTotal = repeatCountTotal;
        this.stopCallback = stopCallback;
        this.repeatTimer = setInterval(
            () => {
                if (this.isPaused) {
                    return;
                }
                let ret;
                try {
                    ret = func(intervalMs);
                } catch (e) {
                    logger.error("repeat定时任务报错 停止定时人物", e.stack)
                    ret = RepeatType.Stop
                }
                ++this.repeatCounter;
                if (this.repeatCountTotal > 0 && this.repeatCounter >= this.repeatCountTotal) {
                    ret = RepeatType.Stop;
                }

                if (ret == RepeatType.Stop) {
                    this.stop();
                } else {
                }
            }
            , intervalMs);
        return this;
    }

    async startDelay(delay: number, delayFinishReturnValue: any): Promise<any> {
        this.stopAllTimer();
        return new Promise<boolean>((resolve, reject) => {
            this.resolveFunc = reject;
            this.oneShot(delay, () => {
                resolve(delayFinishReturnValue);
            })
        })
    }

    pause() {
        this.isPaused = true;
    }

    resume() {
        this.isPaused = false;
    }


    lock = false;
    private checkLockNum = 0;
    private checkLockMaxNum: number;

    enLock(checkLockMaxNum = 30) {
        if (!this.lock) {
            this.lock = true;
            this.checkLockNum = 0;
            this.checkLockMaxNum = checkLockMaxNum
        }
    }

    isLock() {
        if (this.lock) {
            this.checkLockNum++;
            if (this.checkLockNum > this.checkLockMaxNum) {
                this.lock = false;
            }
        }
        return this.lock
    }

    deLock() {
        this.lock = false;
    }

    /**
     * 定时周期事务
     * @param {number} checkLockNum
     * @param {(deLock) => void} callback
     */
    doTransaction(checkLockNum: number, callback: (deLock) => void) {
        if (!this.isLock()) {
            this.enLock(checkLockNum);
            callback(this.deLock.bind(this))
        }
    }
}
