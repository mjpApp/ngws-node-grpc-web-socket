/**
 * 时间
 */
export class Timer{
    static async sleep(time: number){
        return new Promise(resolve => setTimeout(resolve, time));
    }
}
// async function run (){
//     console.log(1)
//     await Timer.sleep(5000)
//     console.log(1)
// }
// run()