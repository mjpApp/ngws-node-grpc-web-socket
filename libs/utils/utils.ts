
import * as os from "os";
import {logger} from "./logs";

export class utils {
    static async doAsync(fun:Function){
        await fun()
    }
    /**
     * 将get地址栏参数转换为对象
     * @param url
     */
    public static parseQueryString (url) {
        var reg_url = /^[^\?]+\?([\w\W]+)$/,
          reg_para = /([^&=]+)=([\w\W]*?)(&|$|#)/g,
          arr_url = reg_url.exec(url),
          ret = {};
        if (arr_url && arr_url[1]) {
            var str_para = arr_url[1], result;
            while ((result = reg_para.exec(str_para)) != null) {
                ret[result[1]] = result[2];
            }
        }
        return ret;
    }

    ///////////////////获取本机ip///////////////////////
    static getIPAdress() {
        let interfaces = os.networkInterfaces();
        for (let devName in interfaces) {
            let iface = interfaces[devName];
            for (let i = 0; i < iface.length; i++) {
                let alias = iface[i];
                if (alias.family === 'IPv4' && alias.address !== '127.0.0.1' && !alias.internal) {
                    return alias.address;
                }
            }
        }
    }
    static async sleep(time:number){
        return new Promise((resolve, reject)=>{
            setTimeout(resolve,time)
        })
    }
}