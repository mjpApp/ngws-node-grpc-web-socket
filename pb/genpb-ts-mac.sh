#protoc --plugin=../node_modules/.bin/protoc-gen-ts_proto --ts_proto_opt=outputServices=nice-grpc,outputServices=generic-definitions,useExactTypes=false --ts_proto_out=./node -I ./proto ./proto/*.proto
#(Note that the output parameter name, ts_proto_out, is named based on the suffix of the plugin's name, i.e.
#"ts_proto" suffix in the --plugin=./node_modules/.bin/protoc-gen-ts_proto parameter becomes the _out prefix, per protoc's CLI conventions.)
#On Windows, use protoc --plugin=protoc-gen-ts_proto=".\\node_modules\\.bin\\protoc-gen-ts_proto.cmd" --ts_proto_out=. ./simple.proto (see #93)
#Ensure you're using a modern protoc (see installation instructions for your platform, i.e. protoc v3.0.0 doesn't support the _opt flag
# --ts_proto_opt="outputServices=nice-grpc,outputServices=generic-definitions,useExactTypes=false"
#https://www.npmjs.com/package/nice-grpc

# node , mac  这个更好nice-grpc ts_proto https://github.com/deeplay-io/nice-grpc/tree/master
../node_modules/.bin/grpc_tools_node_protoc \
  --plugin=protoc-gen-ts_proto=../node_modules/.bin/protoc-gen-ts_proto \
  --ts_proto_out=./node \
  --ts_proto_opt="outputServices=nice-grpc,outputServices=generic-definitions,useExactTypes=false" \
  --proto_path=./proto \
  ./proto/*.proto
# grpc-web
../node_modules/.bin/grpc_tools_node_protoc \
  --plugin=protoc-gen-ts_proto=../node_modules/.bin/protoc-gen-ts_proto \
  --ts_proto_out=./web \
  --ts_proto_opt="env=browser,outputServices=nice-grpc,outputServices=generic-definitions,outputJsonMethods=false,useExactTypes=false" \
  --proto_path=./proto \
  ./proto/*.proto


# window
#../node_modules/.bin/grpc_tools_node_protoc.CMD --plugin="protoc-gen-ts_proto=..\\node_modules\\.bin\\protoc-gen-ts_proto.cmd" --ts_proto_out=".\\node" --ts_proto_opt="outputServices=nice-grpc,outputServices=generic-definitions,useExactTypes=false" --proto_path=".\\proto" ".\\proto\\*.proto"



# grpc-web
#PROTOC_GEN_TS_PATH="../node_modules/.bin/protoc-gen-ts"
#protoc --plugin="protoc-gen-ts=${PROTOC_GEN_TS_PATH}" --ts_opt=json_names --ts_opt=target=web --ts_out="./web" ./proto/*.proto -I ./proto
  # 谷歌
#../node_modules/.bin/grpc_tools_node_protoc \
#    --plugin=protoc-gen-grpc=../node_modules/.bin/grpc_tools_node_protoc_plugin \
#    --plugin=protoc-gen-ts=../node_modules/.bin/protoc-gen-ts \
#    --js_out=import_style=commonjs,binary:./node \
#    --ts_out=grpc_js:./node \
#    --grpc_out=grpc_js:./node \
#    --proto_path=./proto \
#    ./proto/*.proto