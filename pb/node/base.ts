/* eslint-disable */
import * as _m0 from "protobufjs/minimal";

export const protobufPackage = "base";

export enum CodeType {
  /** success - 成功 */
  success = 0,
  /** fail - 失败 */
  fail = 1,
  /** authError - 验证错误 */
  authError = 401,
  /** serverError - 服务逻辑错误 */
  serverError = 500,
  /** paramError - 请求参数错误 */
  paramError = 2,
  UNRECOGNIZED = -1,
}

export function codeTypeFromJSON(object: any): CodeType {
  switch (object) {
    case 0:
    case "success":
      return CodeType.success;
    case 1:
    case "fail":
      return CodeType.fail;
    case 401:
    case "authError":
      return CodeType.authError;
    case 500:
    case "serverError":
      return CodeType.serverError;
    case 2:
    case "paramError":
      return CodeType.paramError;
    case -1:
    case "UNRECOGNIZED":
    default:
      return CodeType.UNRECOGNIZED;
  }
}

export function codeTypeToJSON(object: CodeType): string {
  switch (object) {
    case CodeType.success:
      return "success";
    case CodeType.fail:
      return "fail";
    case CodeType.authError:
      return "authError";
    case CodeType.serverError:
      return "serverError";
    case CodeType.paramError:
      return "paramError";
    case CodeType.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export enum ServiceType {
  nil = 0,
  web = 3000,
  user = 9090,
  UNRECOGNIZED = -1,
}

export function serviceTypeFromJSON(object: any): ServiceType {
  switch (object) {
    case 0:
    case "nil":
      return ServiceType.nil;
    case 3000:
    case "web":
      return ServiceType.web;
    case 9090:
    case "user":
      return ServiceType.user;
    case -1:
    case "UNRECOGNIZED":
    default:
      return ServiceType.UNRECOGNIZED;
  }
}

export function serviceTypeToJSON(object: ServiceType): string {
  switch (object) {
    case ServiceType.nil:
      return "nil";
    case ServiceType.web:
      return "web";
    case ServiceType.user:
      return "user";
    case ServiceType.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

/** 通用的请求 */
export interface req {
  userId: string;
}

/** 通用的返回 */
export interface rsp {
  /** CodeType */
  code: number;
  msg: string;
}

/** 转化包 */
export interface PackAgeReq {
  /** grpc的ServiceDefinition的fullName */
  fullName: string;
  /** grpc的ServiceDefinition的method */
  methodsName: string;
  /** 请求的encode */
  req: Uint8Array;
}

export interface PackAgeRsp {
  fullName: string;
  methodsName: string;
  rsp: Uint8Array;
}

function createBasereq(): req {
  return { userId: "" };
}

export const req = {
  encode(message: req, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.userId !== "") {
      writer.uint32(10).string(message.userId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): req {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBasereq();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.userId = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): req {
    return { userId: isSet(object.userId) ? globalThis.String(object.userId) : "" };
  },

  toJSON(message: req): unknown {
    const obj: any = {};
    if (message.userId !== "") {
      obj.userId = message.userId;
    }
    return obj;
  },

  create(base?: DeepPartial<req>): req {
    return req.fromPartial(base ?? {});
  },
  fromPartial(object: DeepPartial<req>): req {
    const message = createBasereq();
    message.userId = object.userId ?? "";
    return message;
  },
};

function createBasersp(): rsp {
  return { code: 0, msg: "" };
}

export const rsp = {
  encode(message: rsp, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.code !== 0) {
      writer.uint32(8).uint32(message.code);
    }
    if (message.msg !== "") {
      writer.uint32(18).string(message.msg);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): rsp {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBasersp();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.code = reader.uint32();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.msg = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): rsp {
    return {
      code: isSet(object.code) ? globalThis.Number(object.code) : 0,
      msg: isSet(object.msg) ? globalThis.String(object.msg) : "",
    };
  },

  toJSON(message: rsp): unknown {
    const obj: any = {};
    if (message.code !== 0) {
      obj.code = Math.round(message.code);
    }
    if (message.msg !== "") {
      obj.msg = message.msg;
    }
    return obj;
  },

  create(base?: DeepPartial<rsp>): rsp {
    return rsp.fromPartial(base ?? {});
  },
  fromPartial(object: DeepPartial<rsp>): rsp {
    const message = createBasersp();
    message.code = object.code ?? 0;
    message.msg = object.msg ?? "";
    return message;
  },
};

function createBasePackAgeReq(): PackAgeReq {
  return { fullName: "", methodsName: "", req: new Uint8Array(0) };
}

export const PackAgeReq = {
  encode(message: PackAgeReq, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.fullName !== "") {
      writer.uint32(10).string(message.fullName);
    }
    if (message.methodsName !== "") {
      writer.uint32(18).string(message.methodsName);
    }
    if (message.req.length !== 0) {
      writer.uint32(26).bytes(message.req);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): PackAgeReq {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBasePackAgeReq();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.fullName = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.methodsName = reader.string();
          continue;
        case 3:
          if (tag !== 26) {
            break;
          }

          message.req = reader.bytes();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): PackAgeReq {
    return {
      fullName: isSet(object.fullName) ? globalThis.String(object.fullName) : "",
      methodsName: isSet(object.methodsName) ? globalThis.String(object.methodsName) : "",
      req: isSet(object.req) ? bytesFromBase64(object.req) : new Uint8Array(0),
    };
  },

  toJSON(message: PackAgeReq): unknown {
    const obj: any = {};
    if (message.fullName !== "") {
      obj.fullName = message.fullName;
    }
    if (message.methodsName !== "") {
      obj.methodsName = message.methodsName;
    }
    if (message.req.length !== 0) {
      obj.req = base64FromBytes(message.req);
    }
    return obj;
  },

  create(base?: DeepPartial<PackAgeReq>): PackAgeReq {
    return PackAgeReq.fromPartial(base ?? {});
  },
  fromPartial(object: DeepPartial<PackAgeReq>): PackAgeReq {
    const message = createBasePackAgeReq();
    message.fullName = object.fullName ?? "";
    message.methodsName = object.methodsName ?? "";
    message.req = object.req ?? new Uint8Array(0);
    return message;
  },
};

function createBasePackAgeRsp(): PackAgeRsp {
  return { fullName: "", methodsName: "", rsp: new Uint8Array(0) };
}

export const PackAgeRsp = {
  encode(message: PackAgeRsp, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.fullName !== "") {
      writer.uint32(10).string(message.fullName);
    }
    if (message.methodsName !== "") {
      writer.uint32(18).string(message.methodsName);
    }
    if (message.rsp.length !== 0) {
      writer.uint32(26).bytes(message.rsp);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): PackAgeRsp {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBasePackAgeRsp();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.fullName = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.methodsName = reader.string();
          continue;
        case 3:
          if (tag !== 26) {
            break;
          }

          message.rsp = reader.bytes();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): PackAgeRsp {
    return {
      fullName: isSet(object.fullName) ? globalThis.String(object.fullName) : "",
      methodsName: isSet(object.methodsName) ? globalThis.String(object.methodsName) : "",
      rsp: isSet(object.rsp) ? bytesFromBase64(object.rsp) : new Uint8Array(0),
    };
  },

  toJSON(message: PackAgeRsp): unknown {
    const obj: any = {};
    if (message.fullName !== "") {
      obj.fullName = message.fullName;
    }
    if (message.methodsName !== "") {
      obj.methodsName = message.methodsName;
    }
    if (message.rsp.length !== 0) {
      obj.rsp = base64FromBytes(message.rsp);
    }
    return obj;
  },

  create(base?: DeepPartial<PackAgeRsp>): PackAgeRsp {
    return PackAgeRsp.fromPartial(base ?? {});
  },
  fromPartial(object: DeepPartial<PackAgeRsp>): PackAgeRsp {
    const message = createBasePackAgeRsp();
    message.fullName = object.fullName ?? "";
    message.methodsName = object.methodsName ?? "";
    message.rsp = object.rsp ?? new Uint8Array(0);
    return message;
  },
};

function bytesFromBase64(b64: string): Uint8Array {
  if (globalThis.Buffer) {
    return Uint8Array.from(globalThis.Buffer.from(b64, "base64"));
  } else {
    const bin = globalThis.atob(b64);
    const arr = new Uint8Array(bin.length);
    for (let i = 0; i < bin.length; ++i) {
      arr[i] = bin.charCodeAt(i);
    }
    return arr;
  }
}

function base64FromBytes(arr: Uint8Array): string {
  if (globalThis.Buffer) {
    return globalThis.Buffer.from(arr).toString("base64");
  } else {
    const bin: string[] = [];
    arr.forEach((byte) => {
      bin.push(globalThis.String.fromCharCode(byte));
    });
    return globalThis.btoa(bin.join(""));
  }
}

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends globalThis.Array<infer U> ? globalThis.Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}
