# Path to this plugin
PROTOC_GEN_TS_PATH="../node_modules/.bin/grpc_tools_node_protoc_ts"
PROTOC_GEN_GRPC_PATH="../node_modules/.bin/grpc_tools_node_protoc_plugin"
# Directory to write generated code to (.js and .d.ts files)
OUT_DIR="./node"
OUT_DIRweb="./web"
../node_modules/.bin/grpc_tools_node_protoc \
--js_out=import_style=commonjs,binary:${OUT_DIR} \
--grpc_out=grpc_js:${OUT_DIR} \
--plugin=protoc-gen-grpc=${PROTOC_GEN_GRPC_PATH} \
-I ./proto \
./proto/*.proto

# 生成 TypeScript 定义文件
protoc \
--plugin=protoc-gen-ts=../node_modules/.bin/protoc-gen-ts \
--js_out=import_style=commonjs,binary:${OUT_DIR} \
--ts_out=${OUT_DIR} \
-I ./proto \
./proto/*.proto


../node_modules/.bin/dts-gen -m /Users/lili/WebstormProjects/nodegametemplate/pb/node/base