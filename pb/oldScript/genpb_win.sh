# Path to this plugin
PROTOC_GEN_TS_PATH="..\\node_modules\\.bin\\protoc-gen-ts.cmd"
#PROTOC_GEN_GRPC_PATH="..\\node_modules\\.bin\\protoc-gen-grpc.cmd"
PROTOC_GEN_GRPC_PATH="..\\node_modules\\.bin\\grpc_tools_node_protoc_plugin.cmd"
# Directory to write generated code to (.js and .d.ts files)
OUT_DIR=".\\node"
OUT_DIRweb=".\\web"

# About Apple M1 arm64 npm_config_target_arch=x64 npm i grpc-tools
#protoc --plugin="protoc-gen-ts=${PROTOC_GEN_TS_PATH}" --js_out="import_style=commonjs,binary:${OUT_DIR}" --ts_out="${OUT_DIR}" base.proto
#protoc --plugin="protoc-gen-ts=${PROTOC_GEN_TS_PATH}" --ts_out="${OUT_DIR}" base.proto user.proto
#protoc --plugin="protoc-gen-ts=${PROTOC_GEN_TS_PATH}" --ts_out="service=grpc-web:${OUT_DIR}" base.proto user.proto
protoc --plugin="protoc-gen-ts=${PROTOC_GEN_TS_PATH}" --ts_opt=json_names --ts_opt=target=node --ts_out="${OUT_DIR}" .\\proto\\*.proto -I ./proto
protoc --plugin="protoc-gen-ts=${PROTOC_GEN_TS_PATH}" --ts_opt=json_names --ts_opt=target=web --ts_out="${OUT_DIRweb}" .\\proto\\*.proto -I ./proto
#protoc --plugin="protoc-gen-ts=${PROTOC_GEN_TS_PATH}" --plugin="protoc-gen-grpc=${PROTOC_GEN_GRPC_PATH}" --ts_out="service=grpc-node:${OUT_DIR}" --grpc_out="${OUT_DIR}" base.proto user.proto

#https://protobufjs.github.io/protobuf.js/
# proto3的使用
# https://zhuanlan.zhihu.com/p/334617214


