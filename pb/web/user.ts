/* eslint-disable */
import type { CallContext, CallOptions } from "nice-grpc-common";
import * as _m0 from "protobufjs/minimal";
import { PackAgeReq, PackAgeRsp, req, rsp } from "./base";
import Long = require("long");

export const protobufPackage = "user";

export interface User {
  userId: string;
  name: string;
  headImg: string;
  createTime: number;
}

export interface getUserReq {
  req: req | undefined;
}

export interface getUserRsp {
  rsp: rsp | undefined;
  user: User | undefined;
}

export interface userLoginWebReq {
  req: req | undefined;
  user: User | undefined;
}

export interface userLoginWebRsp {
  rsp: rsp | undefined;
  user: User | undefined;
  webToken: string;
}

function createBaseUser(): User {
  return { userId: "", name: "", headImg: "", createTime: 0 };
}

export const User = {
  encode(message: User, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.userId !== "") {
      writer.uint32(10).string(message.userId);
    }
    if (message.name !== "") {
      writer.uint32(18).string(message.name);
    }
    if (message.headImg !== "") {
      writer.uint32(26).string(message.headImg);
    }
    if (message.createTime !== 0) {
      writer.uint32(32).int64(message.createTime);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): User {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseUser();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.userId = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.name = reader.string();
          continue;
        case 3:
          if (tag !== 26) {
            break;
          }

          message.headImg = reader.string();
          continue;
        case 4:
          if (tag !== 32) {
            break;
          }

          message.createTime = longToNumber(reader.int64() as Long);
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  create(base?: DeepPartial<User>): User {
    return User.fromPartial(base ?? {});
  },
  fromPartial(object: DeepPartial<User>): User {
    const message = createBaseUser();
    message.userId = object.userId ?? "";
    message.name = object.name ?? "";
    message.headImg = object.headImg ?? "";
    message.createTime = object.createTime ?? 0;
    return message;
  },
};

function createBasegetUserReq(): getUserReq {
  return { req: undefined };
}

export const getUserReq = {
  encode(message: getUserReq, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.req !== undefined) {
      req.encode(message.req, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): getUserReq {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBasegetUserReq();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.req = req.decode(reader, reader.uint32());
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  create(base?: DeepPartial<getUserReq>): getUserReq {
    return getUserReq.fromPartial(base ?? {});
  },
  fromPartial(object: DeepPartial<getUserReq>): getUserReq {
    const message = createBasegetUserReq();
    message.req = (object.req !== undefined && object.req !== null) ? req.fromPartial(object.req) : undefined;
    return message;
  },
};

function createBasegetUserRsp(): getUserRsp {
  return { rsp: undefined, user: undefined };
}

export const getUserRsp = {
  encode(message: getUserRsp, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.rsp !== undefined) {
      rsp.encode(message.rsp, writer.uint32(10).fork()).ldelim();
    }
    if (message.user !== undefined) {
      User.encode(message.user, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): getUserRsp {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBasegetUserRsp();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.rsp = rsp.decode(reader, reader.uint32());
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.user = User.decode(reader, reader.uint32());
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  create(base?: DeepPartial<getUserRsp>): getUserRsp {
    return getUserRsp.fromPartial(base ?? {});
  },
  fromPartial(object: DeepPartial<getUserRsp>): getUserRsp {
    const message = createBasegetUserRsp();
    message.rsp = (object.rsp !== undefined && object.rsp !== null) ? rsp.fromPartial(object.rsp) : undefined;
    message.user = (object.user !== undefined && object.user !== null) ? User.fromPartial(object.user) : undefined;
    return message;
  },
};

function createBaseuserLoginWebReq(): userLoginWebReq {
  return { req: undefined, user: undefined };
}

export const userLoginWebReq = {
  encode(message: userLoginWebReq, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.req !== undefined) {
      req.encode(message.req, writer.uint32(10).fork()).ldelim();
    }
    if (message.user !== undefined) {
      User.encode(message.user, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): userLoginWebReq {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseuserLoginWebReq();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.req = req.decode(reader, reader.uint32());
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.user = User.decode(reader, reader.uint32());
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  create(base?: DeepPartial<userLoginWebReq>): userLoginWebReq {
    return userLoginWebReq.fromPartial(base ?? {});
  },
  fromPartial(object: DeepPartial<userLoginWebReq>): userLoginWebReq {
    const message = createBaseuserLoginWebReq();
    message.req = (object.req !== undefined && object.req !== null) ? req.fromPartial(object.req) : undefined;
    message.user = (object.user !== undefined && object.user !== null) ? User.fromPartial(object.user) : undefined;
    return message;
  },
};

function createBaseuserLoginWebRsp(): userLoginWebRsp {
  return { rsp: undefined, user: undefined, webToken: "" };
}

export const userLoginWebRsp = {
  encode(message: userLoginWebRsp, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.rsp !== undefined) {
      rsp.encode(message.rsp, writer.uint32(10).fork()).ldelim();
    }
    if (message.user !== undefined) {
      User.encode(message.user, writer.uint32(18).fork()).ldelim();
    }
    if (message.webToken !== "") {
      writer.uint32(26).string(message.webToken);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): userLoginWebRsp {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseuserLoginWebRsp();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.rsp = rsp.decode(reader, reader.uint32());
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.user = User.decode(reader, reader.uint32());
          continue;
        case 3:
          if (tag !== 26) {
            break;
          }

          message.webToken = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  create(base?: DeepPartial<userLoginWebRsp>): userLoginWebRsp {
    return userLoginWebRsp.fromPartial(base ?? {});
  },
  fromPartial(object: DeepPartial<userLoginWebRsp>): userLoginWebRsp {
    const message = createBaseuserLoginWebRsp();
    message.rsp = (object.rsp !== undefined && object.rsp !== null) ? rsp.fromPartial(object.rsp) : undefined;
    message.user = (object.user !== undefined && object.user !== null) ? User.fromPartial(object.user) : undefined;
    message.webToken = object.webToken ?? "";
    return message;
  },
};

export type UserServiceDefinition = typeof UserServiceDefinition;
export const UserServiceDefinition = {
  name: "UserService",
  fullName: "user.UserService",
  methods: {
    webReq: {
      name: "webReq",
      requestType: PackAgeReq,
      requestStream: false,
      responseType: PackAgeRsp,
      responseStream: false,
      options: {},
    },
    userLogin: {
      name: "userLogin",
      requestType: userLoginWebReq,
      requestStream: false,
      responseType: userLoginWebRsp,
      responseStream: false,
      options: {},
    },
    getUser: {
      name: "getUser",
      requestType: getUserReq,
      requestStream: false,
      responseType: getUserRsp,
      responseStream: false,
      options: {},
    },
  },
} as const;

export interface UserServiceImplementation<CallContextExt = {}> {
  webReq(request: PackAgeReq, context: CallContext & CallContextExt): Promise<DeepPartial<PackAgeRsp>>;
  userLogin(request: userLoginWebReq, context: CallContext & CallContextExt): Promise<DeepPartial<userLoginWebRsp>>;
  getUser(request: getUserReq, context: CallContext & CallContextExt): Promise<DeepPartial<getUserRsp>>;
}

export interface UserServiceClient<CallOptionsExt = {}> {
  webReq(request: DeepPartial<PackAgeReq>, options?: CallOptions & CallOptionsExt): Promise<PackAgeRsp>;
  userLogin(request: DeepPartial<userLoginWebReq>, options?: CallOptions & CallOptionsExt): Promise<userLoginWebRsp>;
  getUser(request: DeepPartial<getUserReq>, options?: CallOptions & CallOptionsExt): Promise<getUserRsp>;
}

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends globalThis.Array<infer U> ? globalThis.Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

function longToNumber(long: Long): number {
  if (long.gt(globalThis.Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
