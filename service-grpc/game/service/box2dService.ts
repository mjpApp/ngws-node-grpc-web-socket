
//创建世界
//shipw
//https://box2d.org/documentation/md__d_1__git_hub_box2d_docs_hello.html


import {b2World} from "../../../libs/box2d/src/dynamics/b2_world";
import {b2Vec2} from "../../../libs/box2d/src/common/b2_math";
import {b2ContactListener} from "../../../libs/box2d/src/dynamics/b2_world_callbacks";
import {b2BodyDef} from "../../../libs/box2d/src/dynamics/b2_body";
import {b2CircleShape} from "../../../libs/box2d/src/collision/b2_circle_shape";
import {b2PolygonShape} from "../../../libs/box2d/src/collision/b2_polygon_shape";
import {b2EdgeShape} from "../../../libs/box2d/src/collision/b2_edge_shape";
import {b2FixtureDef} from "../../../libs/box2d/src/dynamics/b2_fixture";
import {b2RevoluteJointDef} from "../../../libs/box2d/src/dynamics/b2_revolute_joint";

/**
 * 物理世界代理 用于碰撞检查
 */

export class Box2dService {
    /** 物理世界根节点 */
    physicsWorld: b2World = null;
    /** 物理世界速度迭代次数 */
    velocityIterations = 1; //6;
    /** 物理世界位置迭代次数 */
    positionIterations = 1; //2;
    overTime = 5;
    stepMaxTime = 0.2;

    /** 创建物理世界 */
    public createPhysicsWorld() {
        if (!this.physicsWorld) {
            //let b = new b2Vec2(0.0, -9.81)
            //let world = new b2World(b)
            let g: b2Vec2 = new b2Vec2(0.0, 0.0);
            this.physicsWorld = new b2World(g);
            // 默认情况下，Box2D会自动清除力
            this.physicsWorld.SetAutoClearForces(true);
            //body->ApplyForce(b2Vec2(10.0f, 0.0f), true); // 假设这是一个持续的力
          // 如果SetAutoClearForces(false)，这个力在下一个时间步仍然有效
          // 如果SetAutoClearForces(true)，这个力在下一个时间步会被清除
        }
    }
    /** 运行模拟 */
    /**
     * Updates the physics simulation by one step.
     *
     * @param {number} dt - The time step for the simulation update.
     * @param {Function} [wakeUpdateFunc] - Optional function to call after the simulation update.
     */
    updateOneStep(dt: number, wakeUpdateFunc?: (dt) => void) {
        this.physicsWorld.Step(dt,
            this.velocityIterations,
            this.positionIterations);
        // Entity physics update
        if (wakeUpdateFunc) {
            wakeUpdateFunc(dt);
        }
    }
    /** 物理世界帧更新函数 */
    updatePhysicsWorld(dt: number, wakeUpdateFunc?: (dt) => void) {
        if (!this.physicsWorld.IsLocked()) {
            if (dt > this.overTime) {
                dt = this.overTime;
            }
            let remainTime = dt;
            while (remainTime > 0) {
                if (this.stepMaxTime < remainTime) {
                    remainTime -= this.stepMaxTime;
                    dt = this.stepMaxTime;
                }
                else {
                    dt = remainTime;
                    remainTime = 0;
                }
                this.updateOneStep(dt, wakeUpdateFunc);
            }
        }
    }

    /** 设置碰撞监听器 */
    setCollisionListener(listener: b2ContactListener) {
        if (this.physicsWorld) {
            this.physicsWorld.SetContactListener(listener);
        }
    }

    /**
     * 启用或禁用
     */
    enable(enable: boolean) {
        if (this.physicsWorld) {
            this.physicsWorld.m_locked = !enable;
        }
    }

    /**  创建刚体 */
    public createOnePhysicsBody(b2Body:b2BodyDef, shape: b2CircleShape|b2PolygonShape|b2EdgeShape){
        /**
         * 创建刚体质量（b2BodyDef）：
         * 首先，你需要创建一个b2BodyDef对象，这是刚体的定义结构。在这个结构中，你可以设置刚体的各种属性，如类型（静态、动态或固定）、位置、角度、线性和角速度、是否允许睡眠等
         */
        //bodyDef.type = b2_dynamicBody; // 动态刚体
        //bodyDef.position.Set(1.0f, 2.0f); // 刚体的初始位置
        //bodyDef.angle = 0.0f; // 刚体的初始角度
        //bodyDef.linearVelocity.Set(0.0f, 0.0f); // 初始线性速度
        //bodyDef.angularVelocity = 0.0f; // 初始角速度
        //bodyDef.allowSleep = true; // 允许刚体进入睡眠状态
        /**
         * 创建形状（b2Shape）：
         * 刚体需要至少一个形状（b2Shape）来定义其物理边界。Box2D提供了多种形状，如圆形（b2CircleShape）、多边形（b2PolygonShape）、边缘（b2EdgeShape）等。创建形状后，你需要将其与b2FixtureDef结构关联，以便将其添加到刚体上。
         */
        //b2CircleShape shape;
        //shape.m_radius = 1.0f; // 创建一个半径为1的圆形
        //
        //b2FixtureDef fixtureDef;
        //fixtureDef.shape = &shape;
        //fixtureDef.density = 1.0f; // 密度
        //fixtureDef.friction = 0.3f; // 摩擦系数
        //fixtureDef.restitution = 0.5f; // 弹性系数
        let fixtureDef = new b2FixtureDef()
        fixtureDef.shape = shape;
        fixtureDef.density = 1.0;
        fixtureDef.friction = 0.3;
        fixtureDef.restitution = 0.5;
        /**
         * （可选）添加关节和约束：
         * 如果需要，你可以在刚体之间添加关节（b2Joint）和约束，以实现更复杂的物理行为。
         *
         * （可选）设置其他属性：
         * 你还可以根据需要设置刚体的其他属性，如质量、惯性等。这些属性可以通过b2Body对象的成员函数进行设置
         */
        let body = this.physicsWorld.CreateBody(b2Body);
        body.CreateFixture(fixtureDef)
    }
    /**  添加约束 */
    addJoint(jointDef:b2RevoluteJointDef){
        /**
         * 创建关节定义（b2JointDef）：
         * 每种类型的关节都有自己的定义类，例如b2RevoluteJointDef用于铰链关节，b2DistanceJointDef用于距离关节等。你需要创建一个关节定义对象，并设置其属性，如关节的类型、连接的刚体、锚点位置等。
         */
        this.physicsWorld.CreateJoint(jointDef)
    }

}