import {
    getUserReq,
    getUserRsp,
    UserServiceImplementation,
    DeepPartial,
    userLoginWebReq, userLoginWebRsp, UserServiceClient, UserServiceDefinition
} from "../../../pb/node/user";
import {CallContext, ServerError, Status, Metadata} from "nice-grpc-common";
import {logger} from "../../../libs/utils/logs";
import {ClientGrpc} from "../../../libs/grpc/clientGrpc";
import {PackAgeReq, PackAgeRsp} from "../../../pb/web/base";
export class UserServiceImpl implements UserServiceImplementation {
    constructor() {
       ClientGrpc.addClientGrpc<UserServiceClient>(UserServiceDefinition);
       //setTimeout(async ()=>{
       //    let rsp1 = await ClientGrpc.getClientGrpc<UserServiceClient>(UserServiceDefinition.fullName).userLogin(userLoginWebReq.create({
       //        user:{
       //            name:"1"
       //        }
       //    }),{metadata: new Metadata().set("userId","1") });
       //    console.log(rsp1)
       //    let rsp2 = await ClientGrpc.getClientGrpc<UserServiceClient>(UserServiceDefinition.fullName).userLogin(userLoginWebReq.create({
       //        user:{
       //            name:"2"
       //        }
       //    }),{metadata: new Metadata().set("userId","2") });
       //    console.log(rsp2)
       //},10000)
    }
    async getUser(request: getUserReq, context: CallContext): Promise<DeepPartial<getUserRsp>> {
        logger.info("getUser",request)
        return getUserRsp.create({});
    }
    async userLogin(request: userLoginWebReq, context: CallContext): Promise<DeepPartial<userLoginWebRsp>> {
        let rsp = userLoginWebRsp.create({
            rsp: {
                code: 0,
                msg: ""
            },
            user: {
                userId: "",
                name: request.user?.name,
                headImg: "",
                createTime: 0
            },
            webToken: ""
        });
        logger.info("登录",request,rsp, context.metadata.get("userId"))
        return rsp
        // return Promise.resolve(undefined);
    }

    async webReq(request: PackAgeReq, context: CallContext): Promise<DeepPartial<PackAgeRsp>> {
        let reqGrpc = UserServiceDefinition.methods[request.methodsName].requestType.decode(request.req)
        let rsp = await this[request.methodsName](reqGrpc,context)
        return PackAgeRsp.create({
            fullName:request.fullName,
            methodsName:request.methodsName,
            rsp: UserServiceDefinition.methods[request.methodsName].responseType.encode(rsp).finish()
        })
    }
}
