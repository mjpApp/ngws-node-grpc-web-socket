import {ServiceType} from "../../pb/node/base";
import {UserServiceDefinition} from "../../pb/node/user";
import {UserServiceImpl} from "./controller/UserServiceImpl";
import {AppGrpc} from "../../libs/grpc/appGrpc";
import {logger} from "../../libs/utils/logs";
export const main = new AppGrpc();
let port = process.env.port || 0
main.run(ServiceType.user, UserServiceDefinition, UserServiceImpl, +port).then(r => {
    logger.info("启动grpc成功")
})



