import {UserServiceClient, UserServiceDefinition} from "../../pb/node/user";
import {ClientGrpc} from "../../libs/grpc/clientGrpc";
import {utils} from "../../libs/utils/utils";

export class GrpcService{
    static initGrpcService(web: string, port: number){
        ClientGrpc.registerWeb(web, port);
        ClientGrpc.addClientGrpc<UserServiceClient>(UserServiceDefinition);
    }
}