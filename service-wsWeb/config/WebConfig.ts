import {UserServiceDefinition} from "../../pb/node/user";
import {PackAgeReq, PackAgeRsp} from "../../pb/node/base";

export class WebConfig{
    static grpcTranMap:{[key:string]: { fullName:string, methods: { webReq:object } } } = {
        [UserServiceDefinition.fullName]:UserServiceDefinition
    }
}