import {Service} from "typedi";
import {Authorized, Body, Controller, CurrentUser, Get, JsonController, Post} from "routing-controllers";
import {PackAgeReq, PackAgeRsp} from "../../../pb/node/base";
import {ClientGrpc} from "../../../libs/grpc/clientGrpc";
import {WebConfig} from "../../config/WebConfig";
import {getUserReq, getUserRsp, User, userLoginWebReq, UserServiceClient, UserServiceDefinition} from "../../../pb/node/user";
import { Metadata } from "nice-grpc";
@Service()
// @Controller(`/grpc`)
@JsonController(`/grpc`)
// @Authorized()
// @Get('current-user')
export class GrpcTransformController {
    // 转发grpc请求和增加用户上下文
    @Post(`/req`)
    public async req(@Body() req: PackAgeReq
                     //, @CurrentUser() user:User
    ):Promise<PackAgeRsp>  {
        if(!WebConfig.grpcTranMap[req.fullName] || !WebConfig.grpcTranMap[req.fullName].methods.webReq){
            return PackAgeRsp.create({
                fullName:"",
                methodsName: "",
                rsp: null
            })
        }
        let definition = WebConfig.grpcTranMap[req.fullName];
        if(ClientGrpc.HasClientGrpc(definition.fullName) && definition.methods[req.methodsName]){
            // 不需要重启web req,再分发到其他请求 todo 会话
            let rsp = await (ClientGrpc.getClientGrpc(definition.fullName) as any).webReq(req,{metadata: new Metadata().set("userId","")})
            return rsp
        }else{
            return PackAgeRsp.create({
                fullName: req.fullName,
                methodsName: "",
                rsp: null
            })

        }
    }
    // grpc 10ms左右
    @Post(`/getUser`)
    public async getUser(@Body() req: getUserReq):Promise<getUserRsp>  {
        return await ClientGrpc.getClientGrpc<UserServiceClient>(UserServiceDefinition.fullName).getUser(req,{metadata: new Metadata().set("userId","2") })
    }
}