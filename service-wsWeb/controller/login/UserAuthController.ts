import 'reflect-metadata';
import {
    Authorized,
    Body,
    Controller,
    CurrentUser,
    Get,
    JsonController,
    Post,
    QueryParam, UseAfter,
    UseBefore
} from 'routing-controllers';
import {TokenHelper} from "../../../libs/utils/tokenHelper";
import {Service} from "typedi";
import {RoutePost} from "../../../libs/utils/routedef";
import {logger} from "../../../libs/utils/logs";
import {setJsonRsp} from "../../../libs/utils/rsp";
import {
    getUserReq,
    getUserRsp,
    User,
    userLoginWebReq,
    userLoginWebRsp,
    UserServiceDefinition
} from "../../../pb/node/user";
import {CodeType} from "../../../pb/node/base";
import {ServiceConst} from "../../../config/ServerConst";

@Service()
@Controller()
@JsonController(`/user`)
export class UserAuthController {
    /** 登录*/
    @Post(`/userLogin`)
    public async userLogin(@Body() req: userLoginWebReq):Promise<userLoginWebRsp>  {
        const user:User = User.create();
        user.name = req.user.name;
        user.headImg = req.user.headImg;
        let findUser:User = {} as User// await UserService.findOneByUser(user);
        let rsp = userLoginWebRsp.create()
        if(!findUser){
            return setJsonRsp(CodeType.fail, rsp)
        }
        try{
            rsp.user = findUser;
            rsp.webToken = TokenHelper.createToken(findUser, ServiceConst.webTokenTimeOut)
            return setJsonRsp(CodeType.success, rsp)
        }catch(e){
            logger.error(e);
            return setJsonRsp(CodeType.serverError, rsp)
        }
    }
    // 测试鉴权
    @Post(`/getUser`)
    @Authorized()
    @Get('current-user')
    async getUser(@Body() req: getUserReq, @CurrentUser() user:User) {
        return setJsonRsp(CodeType.success, getUserRsp.fromJSON({user:user}))
    }
    @Get('/error')
    error(@QueryParam('code') code: number) {
        return setJsonRsp(CodeType.serverError, null)
    }
    
    
}