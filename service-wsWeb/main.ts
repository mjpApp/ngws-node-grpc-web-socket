import "reflect-metadata";
import { useKoaServer,useContainer } from "routing-controllers";
import { Container } from "typedi";
import path from "path";
import {authorizationChecker, currentUserChecker} from "./middleware/auth";
import {ReqLoggingMiddlewareReq, ReqLoggingMiddlewareRsp} from "./middleware/LogsMiddlewares";
import {KoaService} from "./service/koaService";
import {ServiceType} from "../pb/node/base";
import {logger} from "../libs/utils/logs";
import {GrpcService} from "./service/grpcService";
import {WsService} from "./service/wsService";
export class Main {
  private app;
  private koaService: KoaService; //web服务
  private wsService: WsService; // socket服务

  // 端口
  private port: number;
  constructor(port: number) {
    this.port = port
    // typedi 注入到 routing-controllers
    useContainer(Container);
    this.createKoa();
    GrpcService.initGrpcService("web",port)
  }

  /**
   * 创建 koa 服务
   */
  createKoa() {
    this.koaService = new KoaService();
    this.wsService = new WsService()
    this.app = useKoaServer(this.koaService.Koa2, {
      validation: true, // 验证参数@Body({ validate: true }) user: User
      classTransformer: true, // 是否开启类型转换，在获得参数的时候来决定参数形式
      controllers: [`${path.join(__dirname, './controller/**/*.ts')}`],
      authorizationChecker: authorizationChecker, // 配置身份校验
      currentUserChecker: currentUserChecker, // 获取当前用户
      routePrefix: '/api',
      cors: false, // 是否开启跨域请求
      middlewares: [ReqLoggingMiddlewareReq,ReqLoggingMiddlewareRsp], // 配置中间件
      interceptors: [], // 配置拦截器
      defaults: {
        //with this option, null will return 404 by default
        nullResultCode: 404,
        //with this option, void or Promise<void> will return 204 by default
        undefinedResultCode: 204,
        paramOptions: {
          //with this option, argument will be required by default
          required: true,
        },
      },
    });


  }
  // 启动程序
  public run (): any {
    try {
      this.app.listen(this.port, () => logger.info("启动web服务成功 "+this.port));
      this.wsService.listen(this.app)
    } catch (e) {
      logger.error(`❌ 启动出错：${e.message}`);
    }
    this.errorCatch();
  }
  // 错误捕捉Unsupported type
  private errorCatch (): any {
    this.app.on('error', err => {
      logger.error(`❌ 主程序捕捉到错误：${err.message}`);
    });
  }
}
export const webMain = new Main(ServiceType.web);
webMain.run()