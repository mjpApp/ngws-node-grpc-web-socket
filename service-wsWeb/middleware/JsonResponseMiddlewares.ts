import { Interceptor, InterceptorInterface, Action } from "routing-controllers";
import { ResultDataMiddlewares } from "./ResultDataMiddlewares";
import { Service,Inject } from "typedi";
import {logger, Logs} from "../../libs/utils/logs";

// 拦截controller在数据响应给前端，按照标准格式输出数据，同时session存储用户
@Service()
export class JsonResponseInterceptor implements InterceptorInterface {

  @Inject()
  private resultDataMiddlewares: ResultDataMiddlewares;
    
  intercept(action: Action, content: any) {
      if (content == undefined || content.length == 0) {
        logger.error(action);
        return this.resultDataMiddlewares.error(action,"没有数据");
      } else if (content.length != 0 ) {
        return this.resultDataMiddlewares.success(content);
      }
  }
}