import {KoaMiddlewareInterface, Action, Middleware} from "routing-controllers";
import { Service,Inject } from "typedi";
import {logger} from "../../libs/utils/logs";
import moment from "moment";

/**
 * controller 拦截器
 * 拦截 controller 被访问前，进行访问的日志记录
 */
@Service()
export class LogsMiddlewares implements KoaMiddlewareInterface {
  use(context: any, next: (err?: any) => Promise<any>): Promise<any> {
    if (context.request.method == "GET") {
      logger.warn(`${context.request.ip} ${context.request.method} ${context.request.url} ${JSON.stringify(context.request.querystring)} ${JSON.stringify(context.request.header)} \n`);
    } else {
      logger.warn(` ${context.request.ip} ${context.request.method} ${context.request.url} ${JSON.stringify(context.request.body)} ${JSON.stringify(context.request.header)} \n`);
    }
    return next()
  }
}

@Service()
@Middleware({ type: 'before' })
export class ReqLoggingMiddlewareReq implements KoaMiddlewareInterface {
  use(context: any, next: (err?: any) => Promise<any>): Promise<any> {
    if (context.request.method == "GET") {
      logger.debug(`${context.request.method} ${context.request.url} ${JSON.stringify(context.request.querystring)} ${JSON.stringify(context.request.header)} ${context.request.ip} `);
    } else {
      logger.debug(`${context.request.method} ${context.request.url} ${JSON.stringify(context.request.body)} ${JSON.stringify(context.request.header)} ${context.request.ip} `);
    }
    return next();
  }
}
@Service()
@Middleware({ type: 'after' })
export class ReqLoggingMiddlewareRsp implements KoaMiddlewareInterface {
  use(context: any, next: (err?: any) => Promise<any>): Promise<any> {
    logger.debug(`${context.response.status} ${context.request.method} ${context.request.url} ${JSON.stringify(context.response.body)} ${JSON.stringify(context.response.header)} ${context.request.ip} `);
    return next();
  }
}