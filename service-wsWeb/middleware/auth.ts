import {TokenHelper} from "../../libs/utils/tokenHelper";
import { Action, HttpError } from 'routing-controllers';
export function authorizationChecker(action) : boolean {
    // 添加token验证
    const { request, response, next, context } = action;
    const { authorization } = request.headers;
    try{
        const [bearer, token] = authorization.split(' ');
        const user = TokenHelper.decodeToken(token);
        return true;
    }catch(e){
        throw new HttpError(401,'token不正确，解析失败');
    }
    return false
}
export function currentUserChecker(action:Action){
    const { request, response, next, context } = action;
    const { authorization } = request.headers;
    const [bearer, token] = authorization.split(' ');
    const user = TokenHelper.decodeToken(token);
    return user;
}
// https://www.npmjs.com/package/routing-controllers#creating-your-own-koa-middleware
// import { createParamDecorator } from 'routing-controllers';
//
// export function UserFromSession(options?: { required?: boolean }) {
//     return createParamDecorator({
//         required: options && options.required ? true : false,
//         value: action => {
//             const token = action.request.headers['authorization'];
//             return database.findUserByToken(token);
//         },
//     });
// }