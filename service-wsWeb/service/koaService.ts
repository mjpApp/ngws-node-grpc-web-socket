import KoaBodyParser from "koa-bodyparser";
import serve from "koa-static";
import views from "koa-views";
import session from "koa-session";
import path from "path";
import koa from "koa";
import {logger, Logs} from "../../libs/utils/logs";
import {main} from "../../service-grpc/user/main";
import {UserServiceClient, UserServiceDefinition} from "../../pb/node/user";
/**
 * 把第三方插件 和 中间件 的配置从主启动类中剥离处理
 * 便于项目后期配置更多的第三方插件&中间件
 */
export class KoaService {
	// 所有需要配置参数的中间件集合
	public PluginsList: Array<any>;
	public Koa2 = new koa();
	constructor () {
		this.initPlugins();
	}
	/**
	 * 组装koa2的中间件为Array
	 */
	private async initPlugins () {
		// 设置签名的 Cookie 密钥。
		// this.Koa2.keys = [ config.initPlugins.SessionKey ];
		this.PluginsList = [
			Logs.AccessLogger(),
			// 处理post请求
			KoaBodyParser(),
			// 静态资源配置
			// serve(path.join(__dirname, config.initPlugins.staticPath ), { maxage: 0 }),
			// session 配置
			// session({
			// 	key: 'koa:sess',
			// 	maxAge: 86400000,
			// 	overwrite: true,
			// 	httpOnly: true,
			// 	signed: true,
			// 	rolling: false,
			// 	renew: false,
			// },this.Koa2),
			// 模板引擎配置
			// views(path.join(__dirname, config.initPlugins.viewsPath ), {
			// 	extension: 'jade',
			// 	options: { ext: 'jade' }
			// })
		];
		
		try {
			this.PluginsList.forEach((v) => this.Koa2.use(v));
			// @ts-ignore
			// await createConnection(config.typeorm);
		} catch (e) {
			// 终端输出
			logger.error(`配置插件出错：${e.message}`);
		}
	}
}