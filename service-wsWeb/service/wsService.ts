import http from 'http'
const io = require("socket.io")
export class WsService {
    private server
    private socketServer;
    //public listen(app): void {
    //    this.wss = new ws.Server({app})
    //    this.wss.on('connection', function connection(ws) {
    //        ws.on('message', function incoming(message) {
    //            console.log('received: %s', message)
    //        })
    //    })
    //}
    public listen(app) {
        this.server = http.createServer(app.callback())
        // 建立socket服务 这里为什么要把 socketServer export出去? 是为了日后方便在其他服务中调用socket服务推送消息
        this.socketServer = io(this.server)
        // 服务监听socket连接
        this.socketServer.on('connection',(socket) => {
            // 前端建立了连接后要做什么逻辑
            // socket.emit() 是像这个连接进来的socket 发送一个名字为message 的事件,并且附带一个对象{message:"Hello world"} 消息
            socket.emit('message',{message:"Hello world"})
            socket.on('event', data => {
                console.log("event")
            });
            socket.on('disconnect', () => {
                console.log("disconnect")
            });
            socket.on("receive",(info) => {
                console.log("receive")
                // socket.on方法,服务端监听客户端emit的事件，事件名字为 "receive", 并且接收客户端发送过来的信息
            })
        })
    }
}

//const Koa = require('koa')
//const app = new Koa()


//

//let server = app.listen(4000, () => {
//    let port = server.address().port
//    console.log('应用实例，访问地址为 http://localhost:' + port)
//})
//
//const wss = new ws.Server({ server })
//wss.on('connection', function connection(ws) {
//    ws.on('message', function incoming(message) {
//        console.log('received: %s', message)
//    })
//})